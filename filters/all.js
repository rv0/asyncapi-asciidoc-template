const filter = module.exports;

function tree(path) {
    const filteredPaths = path.split('.').filter(Boolean);
    if (!filteredPaths.length) return;
    const dottedPath = filteredPaths.join('.');

    return `${dottedPath}.`;
}
filter.tree = tree;

function buildPath(propName, path) {
    if (!path) return propName;
    return `${path}.${propName}`;
}
filter.buildPath = buildPath;

function isRequired(obj, key) {
    return obj && Array.isArray(obj.required) && !!(obj.required.includes(key));
}
filter.isRequired = isRequired;

function acceptedValues(items) {
    if (!items) return '_Any_';

    return items.map(i => `* \`${i}\``).join('\n');
}
filter.acceptedValues = acceptedValues;

const OPEN_BRACKET_RX = /{/g

function escapeAttributes(string) {
    return string.replace(OPEN_BRACKET_RX, '\\{');
}
filter.escapeAttributes = escapeAttributes;

function byTags(asyncapi) {
    function tags(tagNames, channelInfo, map) {
        (tagNames.length? tagNames : ['untagged']).forEach((tagName) => {
            const channelsForTag = map[tagName]
            if (channelsForTag) {
                channelsForTag.push(channelInfo)
            } else {
                map[tagName] = [channelInfo]
            }
        })
    }
    return Object.entries(Object.entries(asyncapi.channels()).reduce((accum, channelInfo) => {
        const channel = channelInfo[1]
        if (channel.hasPublish()) {
            tags(channel.publish().tagNames(), channelInfo, accum)
        }
        if (channel.hasSubscribe()) {
            tags(channel.subscribe().tagNames(), channelInfo, accum)
        }
        return accum
    }, {})).sort((a,b) => a[0].localeCompare(b[0]))
}
filter.byTags = byTags

const ID_RX = /[/{}]/g
const ID_SEP_RX = /[ -.]/g

function toId(channelName) {
    return `_channel_${channelName.toLowerCase().replace(ID_RX, '').replace(ID_SEP_RX, '_')}`
}
filter.toId = toId
